package algoritmica;

/**
 *
 * @author nacho
 */
public class A1_EjerciciosBuclesMuySimplesResueltos {
    public static void main(String[] args) {
        
        // EJERCICIO 1
        // -----------
        // Escribe los números pares del 1 al 20 separados por espacios
        
        System.out.println("Ejercicio 1");
        System.out.println("-----------");
        for (int i=1; i<=20; i++) {
            System.out.print(i+ " ");
        }
        
        
        // EJERCICIO 2
        // -----------
        // Escribe los números pares del 20 al 1 
        
        System.out.println("\n\nEjercicio 2");
        System.out.println("-----------");
        for (int i=20; i>=1; i--) {  // Cambiamos <= por >= y decrementamos
            System.out.print(i+ " ");
        }     
        
        // EJERCICIO 3
        // -----------
        // Escribe los números impares del 15 al 37 
        
        System.out.println("\n\nEjercicio 3");
        System.out.println("-----------");
        for (int i=15; i<=37; i++) {  
            System.out.print(i+ " ");
        }       
        
        // EJERCICIO 4
        // -----------
        // Escribe los múltiplos de 3 entre 3 y 15 
        
        System.out.println("\n\nEjercicio 4");
        System.out.println("-----------");
        for (int i=3; i<=15; i=i+3) {  
            System.out.print(i+ " ");
        }                 
        // En este caso sabemos que los múltiplos de 3 empiezan en 3 y si voy
        // sumando 3 iré sacándolos
        
        // EJERCICIO 5
        // -----------
        // Escribe los múltiplos de 5 entre 1 y 37 
        
        System.out.println("\n\nEjercicio 5");
        System.out.println("-----------");
        for (int i=5; i<=37; i=i+5) {  
            System.out.print(i+ " ");
        }                 
        // Ahora, aunque me dicen que entre 1 y 37, me da igual. Empiezo en 5
        // que sé que es el primero. El último que mostrará será el 35, porque
        // al sumarle 5 dará 40 y en la siguiente iteración no se cumple i<=37
        // ya que 40<=37 es falso
        
        // EJERCICIO 6
        // -----------
        // Escribe los múltiplos de 7 entre -17 y 45 
        
        System.out.println("\n\nEjercicio 6");
        System.out.println("-----------");
        for (int i=-14; i<=45; i=i+7) {  
            System.out.print(i+ " ");
        }                 
        // Sé que el primer múltiplo es -14 (1x7=7 2x7=14 3x7=21 y ya me paso)
        // cero es un múltiplo correcto porque es múltiplo de cualquier número.
        // Los múltiplos de cualquier número son los números naturales por ese
        // número. Por ejemplo los de 7 son: 7x0=0, 7x1=7, 7x2=14, etc
        // Es decir: 0, 7, 14, etc.
        
        // EJERCICIO 7
        // -----------
        // Escribe los múltiplos de 3 y de 5 entre 0 y 20, primero los de 3
        // y luego los de 5 (por separado)
        
        System.out.println("\n\nEjercicio 7");
        System.out.println("-----------");
        for (int i=0; i<=20; i=i+3) {  
            System.out.print(i+ " ");
        }                         
        System.out.println("");
        for (int i=0; i<=20; i=i+5) {  
            System.out.print(i+ " ");
        }        
        
        // EJERCICIO 8
        // -----------
        // Múltiplos de 11 entre 0 y 50 pero sin incrementar de 11 en 11.
        
        System.out.println("\n\nEjercicio 8");
        System.out.println("-----------");
        for (int i=0; i<=50; i++) {  
            if (i%11==0) {
                System.out.print(i+ " ");
            }
        }                                 
        // Esta vez en lugar de ir avanzando de 11 en 11 empezando por cero
        // vamos recorriendo los 20 números y obtenemos el resto de la división
        // por 11. Es decir el módulo (%) de dividir el número por 11.
        
        // EJERCICIO 9
        // -----------
        // Escribe los múltiplos de 3 y de 5 entre 0 y 20, intercalados.
        
        System.out.println("\n\nEjercicio 9");
        System.out.println("-----------");
        for (int i=0; i<=20; i++) {  
            if (i%3==0 || i%5==0) {
                System.out.print(i+ " ");
            }
        }          
        // Imprimo los que son múltiplos de 3 o múltiplos de 5. Es decir, cuando
        // cumplan una de las dos condiciones. Bien sean de 5 o de 3 los imprimiré
        // Si pusiera && (and) en lugar de || (or) estaría exigiendo que cumpliera
        // las dos cosas a la vez, es decir que fuera a la vez múltiplo de 3 y de 5
        // es decir, múltiplo de 15 y mostraría en este caso únicamente el 15.
        
        
        // EJERCICIO 10
        // ------------
        // Escribe los PRIMEROS 20 múltiplos de 9.
        
        System.out.println("\n\nEjercicio 10");
        System.out.println("------------");
        for (int i=0; i<=19; i++) {  
            System.out.print(i*9 + " ");
        }                  
        // Ahora lo hacemos de otra forma. Como nos dicen los primeros 20, lo que
        // hago es ir multiplicando los primeros 20 naturales (0 al 19) por 9

}

    
}
