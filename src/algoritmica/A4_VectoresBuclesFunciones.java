package algoritmica;

/**
 *
 * @author nacho
 */
public class A4_VectoresBuclesFunciones {
    public static void main(String[] args) {
        
        // ----------
        //  VECTORES
        // ----------
        
        // Un vector o array es una estructura ESTÁTICA (no puede variar de 
        // tamaño en tiempo de ejecución) ORDENADA que nos permite guardar
        // variables (primitivas o instancias de clase) del MISMO TIPO en 
        // posiciones consecutivas de memoria y con un nombre común para todas
        // ellas. Así que para acceder a ellas lo hacemos a través de un índice.
        
        // DEFINICIÓN BÁSICA
        // -----------------
        // int [] numeros = new int[5];
        // String [] palabras = new String [5];
        
        // ASIGNACIÓN
        // ----------
        // numeros[0]=1;
        // numeros[1]=2;
        // palabras[4]="pepito";
        // palabras[2]="jorge"
        
        //             0     1     2     3    4  
        //           -----------------------------
        // numeros  |  1  |  2  |     |     |     |
        //           -----------------------------
        
        
        //               0      1       2       3       4  
        //            ---------------------------------------
        // palabras  |      | jorge |       |       | pepito |
        //            ---------------------------------------
        
        // DEFINICIÓN Y ASIGNACIÓN
        // -----------------------
        // int numeros[]={1, 2, 3, 4, 5}
        // String palabras[]={"pepe", "maria", "jorge", "ana", "eva"}
        
        //             0     1     2     3    4  
        //           -----------------------------
        // numeros  |  1  |  2  |  3  |  4  |  5  |
        //           -----------------------------
        
        
        //               0       1       2       3       4  
        //            ---------------------------------------
        // palabras  |  pepe | maria | jorge |  ana  |  eva  |
        //            ---------------------------------------
        
        // RECORRER UN VECTOR
        // ------------------
        // Lo típico es recorrerlo con un bucle for, ya que sabemos el número
        // de elementos que tiene este (propiedad length).
        
        // EJEMPLO 1
        // ---------
        System.out.println("EJEMPLO 1");
        System.out.println("---------");
        int numeros[]={5, 4, 3, 2, 1}; 
        int tam=numeros.length;
        System.out.println("El vector tiene " + tam + " elementos");
        for (int i=0; i<tam; i++) {
            System.out.print(numeros[i]+ " "); // vamos accediendo a cada una
                                               // de las posiciones mediante
                                               // el índice 
        }
        
        // EJEMPLO 2
        // ---------
        System.out.println("\n\nEJEMPLO 2");
        System.out.println("---------");
        int numeros2[]={1, 4, 7, -2, 5, 5, 5}; 
        tam=numeros2.length;
        for (int i=0; i<tam; i++) {
            // Vamos a imprimir antes la posición de cada variable
            System.out.print(i + "->" + numeros2[i]+ " ");
        }     
        
        // Otra manera de recorrer el vector es mediante un bucle especial
        // que se suele llamar bucle for each (para cada elemento haz tal cosa)
        // for (int elemento:numeros)
        // Sustituimos en cada iteración la variable elemento por el siguiente
        // elemento del vector. En este caso el siguiente entero.
        
        // EJEMPLO 3
        // ---------
        System.out.println("\n\nEJEMPLO 3");
        System.out.println("---------");
        int numeros3[]={1, 4, 7, -2, 5, 5, 5}; 
        tam=numeros3.length;
        for (int elemento:numeros3) {
            System.out.print(elemento + " ");
        }     
        // En este caso no usamos la forma numeros[x] ya que tenemos acceso
        // al elemento de tipo entero directamente.
        
        // Recorrer dos vectores a un tiempo.
        // Otro uso que le podemos dar a los vectores es a modo de registros de
        // una tabla. Es decir, podemos tener por ejemplo, dos vectores: Uno en
        // el que guardaremos nombres y otros en el que guardaremos edades. 
        // Podemos decir entonces que la posición 0 es el registro 0 y que
        // contiene el primer nombre y su correspondiente edad. La posición 1
        // el registro 1, etc. Para ello han de tener el mismo tamaño. Y para
        // saber la edad de un nombre concreto debemos recorrer los dos vectores
        // a la vez (o los tres vectores si añadimos provincia, o los 4, etc.)
        
        // EJEMPLO 4
        // ---------
        // Vamos a imprimir nombre y edad de cada registro. Uno por línea.
        System.out.println("\n\nEJEMPLO 4");
        System.out.println("---------");
        String nombres[]={"Ana", "Pedro", "Jorge", "María"};
        int edades[]={34, 45, 27, 18}; 
        tam=edades.length;
        System.out.println("Mis amigos:");
        // Esta vez no me sirve el bucle for each porque necesito saber
        // la posición en la que me encuentro, así que uso un for normal
        for (int i=0; i<tam; i++) {
            System.out.println("Nombre: " + nombres[i] + "\tEdad: " + edades[i]);
            // El \t es el código para indicar una tabulación. Así quedan a la
            // misma altura
        }             
        
        // Vamos ahora a añadir otro vector con teléfonos y vamos a buscar el
        // teléfono y la edad de Jorge.
        
        // EJEMPLO 5
        // ---------
        System.out.println("\nEJEMPLO 5");
        System.out.println("---------");
        String telefonos[]={"665.435.334", "(+034)677458895", "664 57 38 94", "963 23 54 65"};
        
        // Ahora voy a utilizar distintos bucles para conseguir lo mismo.
        
        // Un bucle for, normal:
        for (int i=0; i<tam; i++) {
            if (nombres[i].equals("Jorge")) {
                System.out.println("El teléfono de Jorge es " + telefonos[i] + " y su edad " + edades[i]);
            }
        }
        // Este bucle el problema que tiene es que no es muy eficiente. Recorre
        // todo el vector siempre. Da igual que el nombre buscado lo encuentre
        // el primero. No le decimos que cuando lo encuentre se salga. Así que
        // si tenemos un millón de registros, aunque encuentre el nombre el
        // primero recorrerá el millón igualmente.
        
        // Para evitar esto vamos a usar ahora la sentencia break que lo que hace
        // es que podamos salirnos de un bucle antes de tiempo cuando queramos
        // nosotros sin tener que esperar a que llegue a la condición límite. El
        // como queramos nosotros se traduce en que usaremos alguna condición
        // adicional de salida mediante algún if. Por ejemplo si encontramos
        // el registro que buscábamos.
        
        for (int i=0; i<tam; i++) {
            if (nombres[i].equals("Jorge")) {
                System.out.println("El teléfono de Jorge es " + telefonos[i] + " y su edad " + edades[i]);
                break; // Ahora ya no se espera a que i<tam. Se sale ya.
            }
        }
        
        // También podemos usar (y, a menudo será lo más conveniente) un bucle
        // while, que siempre es más flexible. También podemos igualmente usar
        // la sentencia break.
        
        int contador=0;
        while (contador<tam) {
            if (nombres[contador].equals("Jorge")) {
                System.out.println("El teléfono de Jorge es " + telefonos[contador] + " y su edad " + edades[contador]);
                break; 
            }            
            contador++;
        }
        
        // Ahora sabemos cómo buscar un teléfono o edad cualquiera a partir de
        // un nombre. Esa es una función que seguramente queramos gastar a
        // menudo. Así que lo lógico sería preguntar cada vez el nombre.
        // Lo mejor en estos casos es hacer una función (la parte de E/S, el
        // pedirlo realmente ya lo haremos cuando haga falta, pero de momento
        // podemos usar una función a la que pasemos un nombre y los vectores
        // y nos devuelva o imprima los datos que buscamos
        
        // EJEMPLO 6
        // ---------
        System.out.println("\nEJEMPLO 6");
        System.out.println("---------");
        String nombres2[]={"Pepe", "Antonio", "Sandra", "Judit"};
        int edades2[]={18, 25, 15, 33};
        escribeEdad("Antonio", nombres2, edades2); // paso los vectores como
                                                   // parámetros sin []
        
        escribeEdad("Rigoberto", nombres2, edades2); // Este no escribe nada
        escribeEdad("Judit", nombres2, edades2);
        escribeEdad2("Rigoberto", nombres2, edades2); // Escribe que no encuentra
    
        // Vamos a hacer ahora una función que nos imprima los nombres y edades
        // de todos los mayores de edad
        
        // EJEMPLO 7
        // ---------
        System.out.println("\nEJEMPLO 7");
        System.out.println("---------");
        // Seguimos con los mismos vectores de antes
        // nombres2: {Pepe", "Antonio", "Sandra", "Judit"}
        // edades2: {18, 25, 15, 33};   
        
        // Vamos a hacer dos funciones una que muestre los mayores de edad
        // y otra que muestre los mayores de cierta edad pasada como parámetro
        escribeMayoresEdad(nombres2, edades2); // Pepe, Antonio y Judit
        escribeMayoresDe(25, nombres2, edades2); // Antonio y Judit
        
        // Cambiamos las edades:
        int edades3[]={12, 14, 15, 11};
        escribeMayoresEdad(nombres2, edades3); 
        escribeMayoresDe(25, nombres2, edades3); 
        
        // RECORRER VECTORES PERO NO NECESARIAMENTE A LA VEZ
        // Hemos visto que es fácil recorrer vectores a la vez y, de esta
        // manera, asociar por la posición elementos de vectores distintos.
        // Vamos ahora a trabajar con dos vectores de enteros y hacer cómo
        // recorrerlos, por ejemplo, para obtener un tercero a partir de los
        // dos anteriores con los elementos intercalados.
        
        // EJEMPLO 8
        // ---------
        System.out.println("\nEJEMPLO 8");
        System.out.println("---------");
        
        int pares[]={0, 2, 4, 6, 8};
        int nones[]={1, 3, 5, 7, 9};
        
        // Definimos un nuevo vector sin inicializar de tamaño la suma de los
        // otros dos vectores.
        int vNumeros[]=new int[pares.length+nones.length];
        
        // Ahora vamos a obtener a partir de los dos primeros los valores del
        // tercer vector. No pueden usar el mismo contador, pues el tercer
        // vector no tiene los mismos elementos. Los dos primeros en este caso
        // sí que pueden ir a la par, aunque no tendría porqué.
        
        // Ante la duda de cuántos elementos tenemos o si simplemente no tengo
        // claro cómo lo haría con un for, uso while (o do que es casi lo mismo)
        
        int contParNon=0;
        int contNumeros=0;
        
        while (contParNon<pares.length) {
            // Por cada paso del contador de los pares y nones
            // damos dos pasos del contador de vNumeros
            vNumeros[contNumeros]=pares[contParNon];
            contNumeros++;
            vNumeros[contNumeros]=nones[contParNon];
            contNumeros++;
            contParNon++;
        }
        
        // Ahora vamos a imprimir vNumeros a ver si es correcto:
        
        System.out.print("{ ");
        for (int elemento: vNumeros) {
            System.out.print(elemento + " ");
        }
        System.out.println("}");
    }
    
    // FUNCIONES
    // ---------
    
    public static void escribeEdad(String nombreBuscado, String[] vNombres, int[] vEdades) {
        // En la definición sí pongo los [] de los vectores, si no no serían vectores
        // Y ahora ya internamente lo resuelvo como antes. Elijo la manera que
        // más me guste y a ser posible sea la más eficiente siempre que necesite
        // eficiencia y no me complique mucho el código
        for (int i=0; i<vNombres.length; i++) {
            if(vNombres[i].equalsIgnoreCase(nombreBuscado)) {
                // Esta vez permito que no importen las mayúsculas/minúsculas
                System.out.println("La edad de " + nombreBuscado + " es " +vEdades[i]);
                break; 
            }
        }
        // Aquí tenemos el pequeño problema de qué pasa si el nombre no existe
        // Tal cuál está ahora no haría nada. No escribiría ningún nombre.
    }
    
    // Vamos ahora a hacer una segunda versión que escriba que no encuentra
    // el nombre en caso de no existir
    public static void escribeEdad2(String nombreBuscado, String[] vNombres, int[] vEdades) {
        // Utilizo un booleano para saber si lo he encontrado o no.
        boolean encontrado=false; // Hasta que no lo encuentre será false
        for (int i=0; i<vNombres.length; i++) {
            if(vNombres[i].equalsIgnoreCase(nombreBuscado)) {
                System.out.println("La edad de " + nombreBuscado + " es " +vEdades[i]);
                // Lo he encontrado, así que cambio el valor de la variable
                encontrado=true;
                // Ahora no podemos salirnos en cuanto encontramos uno, pues
                // pueden haber más, así que no usamos break
            }
        }
        // Ahora ya puedo escribir algo más en función de si ha sido o no encontrado.
        if (! encontrado) {
            System.out.println("El nombre no existe en la Base de datos!!");
        }
    }

    // Considero que en cuanto una persona cumple 18 ya es mayor de 18
    // así que para mí "mayor que 18" es en realidad >=18
    public static void escribeMayoresEdad(String[] vNombres, int[] vEdades) {
        // Utilizo un booleano para saber si hay alguno o no.
        boolean hayMayores=false; // Hasta que no lo encuentre será false
        System.out.println("Mayores de 18 años:");
        // Esta vez lo que busco son edades no nombres
        for (int i=0; i<vEdades.length; i++) {
            if(vEdades[i]>=18) {
                System.out.println(vNombres[i]);
                // Hay alguno mayor de 18, así que cambio el valor de la variable
                hayMayores=true;
            }
        }
        // Ahora ya puedo escribir algo más en función de si ha sido o no encontrado.
        if (! hayMayores) {
            System.out.println("Son todos una panda de niñatos!!");
        }
        System.out.println("");
    }    
    
    // Vuelvo a considerar que "mayor" en realidad aquí es "mayor o igual"
    public static void escribeMayoresDe(int edadLimite, String[] vNombres, int[] vEdades) {
        
        boolean hayMayores=false; // Hasta que no lo encuentre será false

        System.out.println("Mayores de " + edadLimite + " años:");
        
        for (int i=0; i<vEdades.length; i++) {
            if(vEdades[i]>=edadLimite) {
                System.out.println(vNombres[i]);
                hayMayores=true;
            }
        }
        
        if (! hayMayores) {
            System.out.println("No hay ninguna persona tan mayor");
        }
        
        System.out.println("");
    }        
}
