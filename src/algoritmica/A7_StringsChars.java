package algoritmica;

import java.util.StringTokenizer;

/**
 *
 * @author nacho
 */
public class A7_StringsChars {
    public static void main(String[] args) {
        
    // Vamos a ver los métodos y características más importantes de las cadenas.
    // En primer lugar es muy importante recordar cómo se comparan cadenas.
    // Las cadenas no se comparan con "==" sino con el método equals.
    
    // EJEMPLO 1
    // ---------
        System.out.println("Ejemplo 1");
        System.out.println("---------");
        //El contenido del objeto creado es igual al contenido del literal "test":
        System.out.println(new String("test").equals("test")); //->True
        
        //Pero el objeto no es igual al literal:
        System.out.println(new String("test")=="test"); //->False
        
        //Estos dos objetos tienen el mismo contenido pero no son iguales
        System.out.println(new String("test")==new String("test")); //->False
        
        // Dos literales sí se pueden comparar
        System.out.println("test"=="test"); // ->True
        
        // Esto último permite algo inesperado como lo siguiente:
        String cad1="pepe";
        String cad2="pepe";
        System.out.println(cad1==cad2); //->True
        // Aunque no son el mismo objeto sí nos da verdadero por que le hemos
        // asignado valores literales y el compilador se comporta de manera
        // un poco distinta. Y, más adelante, si volvemos a compararlas con '=='
        // podría no dar el resultado esperado
        
        // Así que, resumiendo:
        // Para comparar cadenas y estar SEGURO que se está haciendo bien hay
        // que usar SIEMPRE equals.
        
        // ¿Qué es un char?
        // ----------------
        // Es un caracter (que no una cadena), una letra, pero no es un objeto
        // es un valor entero internamente que equivale al código ASCII o 
        // UNICODE del caracter; del símbolo. El código ASCII de las letras 
        // mayúsculas empieza en 65 (el caracter A)
        // Si a la variable caracter le asignamos un literal entero y lo imprimimos
        // en lugar de mostrar el entero muestra el caracter correspondiente al 
        // entero. Es decir se imprime el caracter correspondiente al código
        // aunque internamente se guarde el código. Si queremos que imprima el
        // código hemos de hacer casting.
        
        // EJEMPLO 2
        // ---------
        System.out.println("\nEjemplo 2");
        System.out.println("---------");
        char car=65;
        System.out.println(car);
        System.out.println((int)car);
        System.out.println((byte)car);
        System.out.println((double)car);
        
        // Si no sabemos el código y simplemente queremos almacenar, por ejemplo,
        // el caracter B en una variable, lo asignamos poniendo B entre comillas
        // pero no dobles sino SIMPLES.

        // EJEMPLO 3
        // ---------
        System.out.println("\nEjemplo 3");
        System.out.println("---------");
        char car2='B';
        System.out.println(car2);
        System.out.println((int)car2);
        System.out.println((byte)car2);
        System.out.println((double)car2);
        
        // Vamos a ver en qué intervalo ASCII están las letras mayúsculas y las
        // minúsculas. Es decir, qué códigos son la A mayúscula, la Z, la a y la z
        // EJEMPLO 4
        // ---------
        System.out.println("\nEjemplo 4");
        System.out.println("---------");        
        System.out.println("El código ASCII de 'A' es " + (int)'A');
        System.out.println("El código ASCII de 'Z' es " + (int)'Z');
        System.out.println("El código ASCII de 'a' es " + (int)'a');
        System.out.println("El código ASCII de 'z' es " + (int)'z');
        // ¿y la ñ?
        System.out.println("El código ASCII de 'Ñ' es " + (int)'Ñ');
        System.out.println("El código ASCII de 'ñ' es " + (int)'ñ');
        // es que como somos raritos y no forma parte del alfabeto inglés la 
        // ponen más adelante.
        
        // ¿Y cómo se comparan caracteres? No existe equals y se comparan como
        // lo que son internamente, o sea, enteros.
        // EJEMPLO 5
        // ---------
        System.out.println("\nEjemplo 5");
        System.out.println("---------");
        System.out.println('A'>'a'); // 65>97 -> False
        System.out.println('a'>'A'); // 97>65 -> True
        System.out.println('A'=='A');
        System.out.println('A'=='a');
        // Podemos usar métodos de caracteres con la clase Character
        // Observa lo que hace el método compare, por ejemplo.
        System.out.println(Character.compare('a', 'A'));
        System.out.println(Character.compare('A', 'A'));
        System.out.println(Character.compare('C', 'A'));
        System.out.println(Character.compare('Z', 'A'));
        System.out.println(Character.compare('A', 'Z'));
        
        // MÉTODOS FUNDAMENTALES DE STRINGS QUE DEBEMOS CONOCER 
        // ----------------------------------------------------
        // EJEMPLO 6
        // ---------
        System.out.println("\nEjemplo 6");
        System.out.println("---------");
        String cadena6="pepito";
        // charAt, devuelve el caracter car que ocupa la posición x
        System.out.println(cadena6.charAt(1));
        // indexOf, devuelve la posición x que ocupa el caracter car (o String)
        // (la primera aparición) y devuelve -1 si no se encuentra en la cadena
        System.out.println(cadena6.indexOf('e')); // caracter e
        System.out.println(cadena6.indexOf('z')); // pos 1
        System.out.println(cadena6.indexOf("pe")); // pos 0
        System.out.println(cadena6.indexOf("pi")); // pos 2
        System.out.println(cadena6.indexOf("pa")); // No está
        

        // EJEMPLO 7
        // ---------
        System.out.println("\nEjemplo 7");
        System.out.println("---------");        
        String cadena7="papapito";
        System.out.println(cadena7.indexOf("pa"));
        System.out.println(cadena7.indexOf("pa", 1));
        System.out.println(cadena7.indexOf("pa", 2));
        System.out.println(cadena7.indexOf("pa", 3));
        System.out.println(cadena7.substring(1, 5));

        // EJEMPLO 8
        // ---------
        System.out.println("\nEjemplo 8");
        System.out.println("---------");        

        int suma=0;
        String cadena8="7;5;6;8;9;2";
        StringTokenizer st=new StringTokenizer(cadena8);
        while (st.hasMoreTokens()) {
            suma+=Integer.parseInt(st.nextToken(";"));
        }
        System.out.println("Suma: " + suma);
        
        String cadena82="7 5 6 8 9 2";
        suma=0;
        st=new StringTokenizer(cadena82);
        while (st.hasMoreTokens()) {
            suma+=Integer.parseInt(st.nextToken());
        }
        System.out.println("Suma: " + suma);        
    }
    
}
