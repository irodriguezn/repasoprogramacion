package algoritmica;

/**
 *
 * @author nacho
 */
public class A2_EjemplosBuclesWhile {
    public static void main(String[] args) {
        
    //  BUCLES SIMPLES (CONTINUACIÓN)
    
    /*  Vamos a ver más en detalle qué es lo que hace el bucle for
        Lo que hacemos es:
        
        *   Darle un VALOR INICIAL a una variable (típicamente i, j, k)
        *   Indicarle un INCREMENTO O DECREMENTO (i++, i=i+4, i--, i=i-3)
        *   Indicarle la CONDICIÓN QUE DEBE CUMPLIRSE para ENTRAR al bucle
        *   Al finalizar una iteración (tras realizar todas las sentencias 
            dentro del bucle) INCREMENTAMOS O DECREMENTAMOS la variable tantas
            unidades como indicamos en la sentencia inicial.
        *   Se realiza con el nuevo valor la COMPARACIÓN, la CONDICIÓN y, sí
            se cumple, es cuando entramos de nuevo a hacer otra iteración.
            
        Por ejemplo: for (int i=0; i<=10; i++)
        
            Paso 1- Le asignamos el valor 0 a la variable i (i=0)
            Paso 2- Comprobamos que se cumple la condición inicial
                    Es decir: ¿Se cumple (es verdadero) que 0<10?
            Paso 3- Como en este caso sí que se cumple entramos al interior
                    del bucle y realizamos las sentencias, las instrucciones
                    que hayan.
            Paso 4- Una vez llegado a la llave final, incrementamos la variable
                    (o decrementamos): i=i+1 -> i=1
            Paso 5- Volvemos a ver si se cumple la condición de entrada
                    ¿Es 1<10? -> Sí, luego entramos de nuevo.
            Paso x- Cuando hemos hecho ya varias iteraciones i se incrementa y 
                    vale 10. Como se cumple que 10<=10 entramos de nuevo y se
                    incrementa a 11. Como 11<=10 es falso ya no entramos más
                    y se continúa la ejecución, después de la llave de cierre.
        
        
        --------------------------------------------------------
        --------------------- BUCLE WHILE ----------------------
        --------------------------------------------------------
        
        Vamos a ver ahora lo que hace un bucle While
        
        Lo que hace while es comprobar una condición y, si se cumple, entonces
        realizar las instrucciones dentro del bucle.
        
        Es muy parecido, en realidad, al bucle for, pero mucho más simple y, a
        la vez, más flexible y potente.
        
        La diferencia principal es que SÓLO COMPRUEBA UNA CONDICIÓN DE ENTRADA.
        No estamos obligados a inicializar una variable ni a incrementarla.
        Esto lo hace más flexible pero a la vez más peligroso: 
        Tenemos que controlar nosotros las variables que queramos usar para 
        comprobar la condición. Es decir, no se incrementa sola, ni se 
        inicializa sola la variable que utilicemos para evaluar la condición de 
        entrada en el bucle.
        
        Si queremos utilizarlo de manera simple, como si fuera un for, DEBEMOS
        INICIALIZAR una variable ANTES de comprobar la condición de entrada en
        el bucle. Y DEBEMOS INCREMENTAR (o decrementar) la variable DENTRO del 
        bucle. Si no lo hacemos podemos crear sin querer un BUCLE INFINITO.
        
        Así, por ejemplo, el equivalente a un: for (int i=1; i<=10; i++) sería:
        
        int i=1; // INICIALIZAMOS ANTES DEL WHILE
        while (i<=10) {
            // Hacemos lo que sea
            i++; // INCREMENTAMOS DENTRO DEL WHILE
        }
        
        Veamos ahora unos cuantos ejemplos
        
    */
        
        // EJEMPLO SIMPLE 1
        // ----------------
        // Vamos a imprimir 10 veces "hola"
        System.out.println("Ejemplo 1");
        System.out.println("---------");
        int i=1;
        while (i<=10) {
            System.out.print("Hola "+" ");
            i++;
        }        
        
        // EJEMPLO SIMPLE 2
        // ----------------
        // Vamos a imprimir 10 veces "hola" inicializando por 7
        System.out.println("\n\nEjemplo 2");
        System.out.println("---------");
        i=7; // Al estar fuera del bucle sólo declaramos el tipo int una vez
        while (i<=16) {
            System.out.print("Hola "+" ");
            i++;
        }        
        
        // EJEMPLO SIMPLE 3
        // ----------------
        // Vamos a imprimir múltiplos de 3 entre 0 y 20
        System.out.println("\n\nEjemplo 3");
        System.out.println("---------");
        i=0; 
        while (i<=19) {
            System.out.print(i +" ");
            i=i+3; // o lo que sería lo mismo i+=3
        }                

        
        // EJEMPLO SIMPLE 4
        // ----------------
        // Vamos a imprimir los 20 primeros múltiplos de 4
        System.out.println("\n\nEjemplo 4");
        System.out.println("---------");
        i=0; 
        while (i<=19) {
            System.out.print(3*i +" ");
            i++;
        }                
        
        // EJEMPLO SIMPLE 5
        // ----------------
        // Vamos a imprimir los 20 primeros múltiplos de 3 o 4 (INCORRECTO)
        System.out.println("\n\nEjemplo 5");
        System.out.println("---------");
        i=0; 
        while (i<=19) {
            if (i%3==0 || i%4==0) {
                System.out.print(i +" ");
            }
            i++;
        }    
        // ¿Es esto correcto? ¿Por qué no? ¿Qué ha pasado?
        // Hemos sacado los múltiplos de 3 y 4 entre el 0 y el 20
        // ¿Cómo podemos sacar los 20 primeros si no sabemos cuál sería el
        // límite? Solución: Contar
        // La condición de salida esta vez será independiente de la variable
        // i que estábamos usando hasta ahora. Cada variable irá por su cuenta.

        // EJEMPLO SIMPLE 6
        // ----------------
        // Vamos a imprimir los 20 primeros múltiplos de 3 o 4 (CORRECTO)
        System.out.println("\n\nEjemplo 6");
        System.out.println("---------");
        i=0; 
        int contador=0; //Vamos a contar múltiplos y aún no hemos impreso ninguno
        while (contador<=19) {
            if (i%3==0 || i%4==0) {
                System.out.print(i +" ");
                contador++; //Si se cumple la condición, ya tenemos un múltiplo
                            //así que incrementamos la variable y no tiene porqué
                            //coincidir con la variable i
            }
            i++; // i se incrementa siempre para ir probando números
                 // para comprobar si son o no los múltiplos buscados
                 // contador se incrementa sólo cuando se da la condición de que
                 // es múltiplo de 3 o de 5. Así que i se hará más grande que 
                 // contador
        }            
        // En este ejemplo ya empezamos a comprobar la flexibilidad que nos
        // proporciona el bucle while respecto al for.
        // CUANDO NO TENEMOS CLARA LA CONDICIÓN DE FIN, CUANDO NO SABEMOS EL
        // NÚMERO DE VECES QUE VAMOS A HACER ALGO USAMOS WHILE EN VEZ DE FOR.
        
        // EJEMPLO SIMPLE 7
        // ----------------
        // Obtener la serie aritmética (incremento cte): 5-7-9-11-13-15-17-19...
        System.out.println("\n\nEjemplo 7");
        System.out.println("---------"); 
        // Con un bucle for
        for (i=5; i<=19; i+=2) {
            System.out.print(i+" ");
        }
        System.out.println("");
        // Con while
        i=5;
        while (i<=19) {
            System.out.print(i+" ");            
            i+=2;
        }
        
        // EJEMPLO 8
        // ---------
        // Obtener los primeros 10 elementos de la serie: 5-7-9-11-13-15-17...
        System.out.println("\n\nEjemplo 8");
        System.out.println("---------"); 
        i=5;
        contador=1; //Esta vez cuento desde 1 en lugar de cero, por cambiar.
        while (contador<=10) { // Pongo 10 y no 9
            System.out.print(i + " ");
            i+=2; // Aumento i de 2 en 2
            contador++; //En cada iteración obtengo un elemento de la serie
        }
        
        // EJEMPLO 9
        // ---------
        // Obtener la suma de los 10 primeros elementos de la serie: 1-4-7-10...
        System.out.println("\n\nEjemplo 9");
        System.out.println("---------"); 
        i=1;
        contador=1;
        int suma=0;
        while (contador<=10) {
            System.out.print(i + " "); //No me lo piden pero los imprimo
            suma+=i; // Voy acumulando los elementos en la variable suma
            i+=3; // Aumento i de 3 en 3
            contador++; //En cada iteración obtengo un elemento de la serie
        }        
        System.out.print("\nLa suma es " + suma);
        // ¿Qué pasaría si sumara después de incrementar?
        
        // EJEMPLO 10
        // ----------
        // Obtener el término undécimo de la serie: 1-3-5-7...
        System.out.println("\n\nEjemplo 10");
        System.out.println("----------"); 
        i=1;
        contador=1;
        while (contador<=11) {
            System.out.print(i + " "); //No me lo piden pero los imprimo
            if (contador==11) {
                System.out.println("\nEl elemento undécimo de la serie es " + i);
            }
            i+=2; 
            contador++;
        }        
        // ¿Sería correcto poner el print después de incrementar las variables?
        // Una manera mucho más fácil es utilizar la fórmula 2*(n-1)+1
        System.out.println("\nEl elemento undécimo de la serie es " + (2*10+1));
        // El enésimo elemento de 1-7-13-19 (incremento 6) 6*(n-1)+1
        // 2º elemento= 6*(2-1)+1=6+1=7 | 3º= 6*(3-1)+1=13 etc.
        // En realidad el +1 es porque estamos empezando en 1.
        // Si empezamos en 5 la serie: 5-10-15-20
        // El elemento 3 sería: 5*(3-1)+5=15 
        // La fórmula es: diferencia * (enésimo - 1) + número inicial
       
    }
}
