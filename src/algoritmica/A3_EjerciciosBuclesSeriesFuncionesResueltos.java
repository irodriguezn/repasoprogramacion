package algoritmica;

/**
 *
 * @author nacho
 */
public class A3_EjerciciosBuclesSeriesFuncionesResueltos {
    public static void main(String[] args) {
        
        // EJERCICIO 1
        // -----------
        // Escribe el elemento que hace 10 de la serie: 7-15-23-31...
        // Hazlo con un bucle while y un contador
        // Hazlo aplicando la fórmula de las sucesiones aritméticas
        // Imprime también la serie con cada elemento precedido de su
        // posición: 1->7 2->15 3->23 ... 
        
        System.out.println("Ejercicio 1");
        System.out.println("-----------");
        int i=7;
        int contador=1;
        while (contador<=10) {
            System.out.print(contador + "->" + i + " ");
            if (contador==10) {
                System.out.println("\nEl décimo elemento de la serie es: " + i);
            }
            i+=8;
            contador++;
        }
        System.out.println("El décimo elemento de la serie es: " + (8*(10-1)+7));
        
    
        // EJERCICIO 2
        // -----------
        // Escribe el intervalo numérico: 12-16-20-24-28-32
        // Hazlo con un bucle for con el "paso" o incremento necesario
        // hazlo con un for (i=1; i<=6; i++)
        
        System.out.println("\nEjercicio 2");
        System.out.println("-----------");
        for (i=12; i<=32; i+=4) {
            System.out.print(i+" ");
        }
        System.out.println("");
        for (i=1; i<=6; i++) {
            System.out.print((i*4+8)+" "); // Hay que multiplicar por 4, ver lo
                                           // que nos da i*4 y sumar lo que
                                           // haga falta para 12
                                           // 4+8=12 -> Luego 8 hemos de sumar
        }
        
        // EJERCICIO 3
        // -----------
        // Escribe el intervalo numérico: -11 -6 -1 4 9 14 19
        // Hazlo con un bucle for con el "paso" o incremento necesario
        // hazlo con un for de paso 1
        
        System.out.println("\n\nEjercicio 3");
        System.out.println("-----------");
        for (i=-11; i<=19; i+=5) {
            System.out.print(i+" ");
        }
        System.out.println("");
        for (i=1; i<=7; i++) {
            System.out.print((i*5-16)+" ");
        }
        
        // EJERCICIO 4
        // -----------
        // Escribe el intervalo numérico: 15 9 3 -3 -9 -15
        // Hazlo con un bucle for con el "paso" o incremento necesario
        // hazlo con un for de paso -1
        
        System.out.println("\n\nEjercicio 3");
        System.out.println("-----------");
        for (i=15; i>=-15; i-=6) {
            System.out.print(i+" ");
        }
        System.out.println("");
        for (i=6; i>=1; i--) {
            System.out.print((i*6)-21+" ");
        }        

        // EJERCICIO 5
        // -----------
        // Escribe el intervalo numérico: 45 23 1 -21 -28 -43 -65
        // Hazlo con un bucle for con el "paso" o incremento necesario
        // hazlo con un for de paso 1
        
        System.out.println("\n\nEjercicio 5");
        System.out.println("-----------");
        for (i=45; i>=-65; i-=22) {
            System.out.print(i+" ");
        }
        System.out.println("");
        for (i=1; i<=6; i++) {
            System.out.print((i*-22+67)+" ");
        }
        
        // EJERCICIO 6
        // -----------
        // Escribe el elemento que hace 5 de la serie: 12 19 26...
        // Hazlo con un bucle while y un contador
        // Hazlo aplicando la fórmula de las sucesiones aritméticas
        // Imprime también la serie con cada elemento precedido de su
        // posición: 1->12 2->19 3->26 ... 
        
        System.out.println("\n\nEjercicio 6");
        System.out.println("-----------");
        i=12;
        contador=1;
        while (contador<=5) {
            System.out.print(contador + "->" + i + " ");
            if (contador==5) {
                System.out.println("\nEl quinto elemento de la serie es: " + i);
            }
            i+=7;
            contador++;
        }
        System.out.println("El quinto elemento de la serie es: " + (7*(5-1)+12));  
        
        
        // EJERCICIO 7
        // -----------
        // Haz una función que devuelva los primeros 10 elementos de una serie
        // con un incremento aleatorio entre 1 y 10
        
        System.out.println("\nEjercicio 7");
        System.out.println("-----------");
        serieRand10();
        
        // EJERCICIO 8
        // -----------
        // Haz una función que devuelva los N elementos de una serie con
        // incremento M

        System.out.println("\n\nEjercicio 8");
        System.out.println("-----------");
        serieNM(10,3);
        
        // EJERCICIO 9
        // -----------
        // Haz una función que devuelva los N elementos de una serie pasados
        // como parámetro con un incremento aleatorio entre 1 y 10
        
        System.out.println("\n\nEjercicio 9");
        System.out.println("-----------");
        serieRandN(20);     
        

        // EJERCICIO 10
        // ------------
        // Haz una función que devuelva los N elementos de una serie pasados
        // como parámetro con un incremento aleatorio entre -M y M
        // el primer elemento será 1 o -1 según crezca o decrezca.
        
        System.out.println("\n\nEjercicio 10");
        System.out.println("------------");
        serieRandNM(10,10);
}    

    public static void serieRand10() {
        int incremento=(int)(Math.random()*10+1);
        // Esta vez lo voy a resolver sin contar elementos, simplemente
        // multiplicando el límite superior por el incremento
        for (int i=1; i<=10*incremento; i+=incremento) {
            System.out.print(i+" ");
        }
        // Puedes probar a hacerlo con contador
    }
    
    public static void serieNM(int n, int m) {
        System.out.println("N="+n+" Incremento="+m);
        for (int i=1; i<=n*m; i+=m) {
            System.out.print(i+" ");
        }
    }
    
    // Incremento aleatorio entre 1 y 10. N elementos.
    public static void serieRandN(int n) {
        int incremento=(int)(Math.random()*10+1);
        System.out.println("N="+n+" Incremento="+incremento);
        for (int i=1; i<=n*incremento; i+=incremento) {
            System.out.print(i+" ");
        }
    }    
    
    // n - número de elementos de la serie. m-incremento aleatorio entre -m y m
    public static void serieRandNM(int n, int m) {
        // si m es 5 el intervalo puede estar entre -5 y +5
        // tenemos que sacar un número aleatorio entre -5 y 5
        // 5 - (-5) = 10 + 1 (el cero tb cuenta) = 11 elementos
        // Hemos de escoger entre estos elementos (-5 -4 -3 -2 -1 0 1 2 3 4 5)
        int elementos=m*2+1; // 5*2 + 1
        // Está claro que debemos multiplicar por elementos, pero, qué hemos
        // de sumar para que empiece en -5? 
        // random() devuelve un número entre 0 y 1. Lo más bajo que puede salir
        // es cero que multiplicado por elementos es cero. Así que simplemente
        // hemos de restarle 5 (bueno, m)
        int incremento=(int)(Math.random()*elementos-m); 
        
        System.out.println("N="+n+" M="+m+ " Incremento="+incremento);
        // Ahora tenemos el incremento, pero dependiendo de si es positivo
        // o negativo o cero el bucle será ascendente o descendente o 
        // no hará nada (incremento 0).
        if (incremento>=0) {
            for (int i=1; i<=n*incremento; i+=incremento) {
                System.out.print(i+" ");
            }
        } if (incremento<0) {
            for (int i=-1; i>=n*incremento; i+=incremento) {
                System.out.print(i+" ");
            }
        }
    }
    
}
