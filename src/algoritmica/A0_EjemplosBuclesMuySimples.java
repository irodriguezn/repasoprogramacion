
package algoritmica;

/**
 *
 * @author nacho
 */
public class A0_EjemplosBuclesMuySimples {
    public static void main(String[] args) {
        
    //  EJERCICIOS MUY SIMPLES DE BUCLES
    
    /*  --------------------------------------------------------------
        --------------------------------------------------------------
        BUCLE FOR (PARA I=1 HASTA 10 - for (int i=1; i<=10; i++) {}
        --------------------------------------------------------------
        --------------------------------------------------------------
        Se utiliza para repetir sentencias encerradas dentro del bucle. 
        Se usa cuando SABEMOS CUÁNTAS VECES vamos a hacer algo.
        Por ejemplo, si quiero imprimir 10 veces Hola, ejecutaré un bucle
        desde i=1 hasta 10
        
        Es típico su uso para recorrer vectores y matrices.
        Por ejemplo, para mostrar los elementos de un vector de 10 enteros
        haría lo siguiente.
        int vector[]={1,1,2,2,3,3,4,4,5,5};
        for (int i=0; i<vector.length(); i++){
            System.out.print(vector[i] + " ");
        }
    
        Lo que hacemos en realidad con un bucle for es darle un intervalo
        de valores a una variable y repetir las sentencias de dentro del
        bucle tantas veces como elementos tenga el intervalo.
    
        Cuando decimos for(int i=1; i<=10; i++) lo que estamos haciendo es
        que, en cada iteración del bucle, i vaya aumentando su valor desde
        1 hasta 10. Y en cada iteración hacemos las sentencias internas al
        bucle. Así que en este caso haremos 10 veces lo que haya en el bucle
        y la variable i irá tomando en cada iteración los valores del 1 al
        10. Estos valores podemos o no usarlos, pero lo que está claro es
        que haremos 10 iteraciones. Hasta que el valor de i no supere el 10
        no nos saldremos del bucle.
    
        Vamos a ver unos cuantos ejemplos
    
    */
    
        // EJEMPLO MUY SIMPLE 1
        // --------------------
        // Vamos a imprimir 3 veces "hola"
        System.out.println("Ejemplo 1");
        System.out.println("---------");
        for (int i=1; i<=3; i++) {
            System.out.println("Hola");
        }
        // En este ejemplo i va tomando valores del 1 al 3 y hasta que no
        // vale 4 no salimos del bucle. Realmente con la variable i, en este 
        // caso, no estamos haciendo nada, pero hemos imprimido 3 veces hola
        // porque vamos dando valores desde el 1 hasta el 3, es decir hacemos
        // 3 veces algo
        
        // EJEMPLO MUY SIMPLE 2
        // --------------------
        // Igual que antes pero el inicio y el final del intervalo son otros
        System.out.println("\nEjemplo 2"); // \n es un salto de linea
        System.out.println("---------");
        for (int i=25; i<=27; i++) {
            System.out.println("Hola");
        }
        // En este ejemplo vamos dando valores a i desde 25 hasta 27 y, cuando
        // llega a 28 ya no vuelve a entrar en el bucle. Así que en total
        // imprime de nuevo 3 veces hola. Da igual los valores de i porque
        // no hacemos nada con ellos.
        
        // EJEMPLO MUY SIMPLE 3
        // --------------------
        // Para ver que la variable i va tomando valores vamos a imprimirla
        // a continuación del hola
        System.out.println("\nEjemplo 3"); 
        System.out.println("---------");   
        for (int i=1; i<=3; i++) {
            System.out.println("hola" + i);
        }
        // Sale el valor de i en cada iteración del bucle a continuación del
        // hola. Con el más "+" concatenamos, es decir, se toma el entero i
        // como si de una cadena se tratase y se añade a continuación de "hola".
        
        // EJEMPLO MUY SIMPLE 4
        // --------------------
        // Volvemos a hacer lo mismo con otro intervalo de i
        System.out.println("\nEjemplo 4"); 
        System.out.println("---------");   
        for (int i=3; i<=5; i++) {
            System.out.println("hola" + i);
        }        
        
        // EJEMPLO MUY SIMPLE 5
        // --------------------
        // Ahora sólo imprimimos la variable i (los valores que va tomando)
        System.out.println("\nEjemplo 5"); 
        System.out.println("---------");   
        for (int i=3; i<=5; i++) {
            System.out.println(i);
        }                
        
        // EJEMPLO MUY SIMPLE 6
        // --------------------
        // Ahora lo mismo pero sin salto de línea. Todos en la misma linea
        System.out.println("\nEjemplo 6"); 
        System.out.println("---------");   
        for (int i=3; i<=5; i++) {
            System.out.print(i); // Simplemente le quitamos el ln
        }       
        
        // EJEMPLO MUY SIMPLE 7
        // --------------------
        // Ahora vamos a imprimir 3 asteriscos en lugar de la variable
        System.out.println("\n\nEjemplo 7"); 
        System.out.println("---------");   
        for (int i=3; i<=5; i++) {
            System.out.print("*");
        }    
        
        // EJEMPLO MUY SIMPLE 8
        // --------------------
        // Da lo mismo que el bucle sea negativo
        System.out.println("\n\nEjemplo 8"); 
        System.out.println("---------");   
        for (int i=-5; i<=-3; i++) {
            System.out.print("*");
        }               
        // Lo único es que -5 es menor que -3. El intervalo es desde -5 hasta
        // -3. Al revés no haría nada porque si partiéramos de -3, la comparación
        // -3<=-5 no se cumpliría y no haríamos ninguna iteración
        
        // EJEMPLO MUY SIMPLE 9
        // --------------------
        // Lo que sí que podemos es hacer un bucle descendente
        // Le decimos que el intervalo es de 3 a 1
        // Para ello la comparación ahora es un > o >= en lugar de < o <=
        // y en lugar de incrementar i, lo decrementamos
        System.out.println("\n\nEjemplo 9"); 
        System.out.println("---------");   
        for (int i=3; i>=1; i--) {
            System.out.print(i + " "); //Separo esta vez los números con espacios
        }      
        
        // EJEMPLO MUY SIMPLE 10
        // ---------------------      
        // También podemos avanzar de más en más. Podemos incrementar la 
        // variable un número distinto de 1. Por ejemplo, si queremos mostrar
        // los números pares entre 0 y 10 (ambos incluidos):
        System.out.println("\n\nEjemplo 10"); 
        System.out.println("----------");   
        for (int i=0; i<=10; i=i+2) {
            System.out.print(i + " "); 
        }              
    }
    
}


