package algoritmica;

/**
 *
 * @author nacho
 */
public class A5_EjerciciosVectoresBuclesResueltos {
    public static void main(String[] args) {
        
        // EJERCICIO 1
        // -----------
        // Escribe una función a la que se le pasen como parámetros dos 
        // vectores de igual tamaño y los intercale. Un elemento del primero
        // y un elemento del segundo. Haz también un método que muestre vectores.
        // Ejemplo: 
        // Entrada: v1 {1, 3, 9, 7} y v2 {2, 6, 8, 1}
        // Salida: v3 {1, 2, 3, 6, 9, 8, 7, 1}

        System.out.println("Ejercicio 1");
        System.out.println("-----------");
        int[] v11={1, 3, 9, 7};
        int[] v12={2, 6, 8, 1};
        int[] v;
        v=intercala(v11, v12);
        muestra(v);

        // EJERCICIO 2
        // -----------
        // Lo mismo que antes pero con vectores de distinto tamaño. Ejemplo: 
        // Entrada: v1 {1, 3, 9, 7} y v2 {2, 6, 8, 1, 3}
        // Salida: v3 {1, 2, 3, 6, 9, 8, 7, 1, 3}

        System.out.println("\nEjercicio 2");
        System.out.println("-----------");
        int v21[]={1, 3, 9, 7};
        int v22[]={2, 6, 8, 1, 3};
        v=intercala2(v21, v22);
        muestra(v);
        
        // EJERCICIO 3
        // -----------
        // Escribe una función a la que se le pasen dos vectores del tamaño que 
        // sea con sus elementos ORDENADOS DE MENOR A MAYOR y se conviertan en 
        // uno solo pero con los elementos en el orden correcto. Ejemplo:
        // Entrada: v1 {1, 12, 14, 23, 45, 87} y v2 {3, 5, 7, 15, 23, 34, 45, 84, 103}
        // Salida: v3 {1, 3, 5, 7, 12, 14, 15, 23, 23, 34, 45, 45, 84, 87, 103}
        System.out.println("\nEjercicio 3");
        System.out.println("-----------");
        int v31[]={1, 12, 14, 23, 45, 87};
        int v32[]={3, 5, 7, 15, 23, 34, 45, 84, 103};
        v=joinSortedV(v31, v32);
        muestra(v);
        
        // EJERCICIO 4
        // -----------
        // Escribe una función a la que se le pasen dos vectores de igual tamaño
        // y devuelva otro también del mismo tamaño cuyos elementos correspondan
        // a la suma de cada uno de los elementos de los otros vectores.
        // Entrada: v1 {1, 3, 2, 7, 5, 5} v2 {3, 5, 1, 8, 3, 3}
        // Salida: v3 {4, 8, 3, 15, 8, 8}
        System.out.println("\nEjercicio 4");
        System.out.println("-----------");
        int v41[]={1, 3, 2, 7, 5, 5};
        int v42[]={3, 5, 1, 8, 3, 3};
        v=sumaV(v41, v42);
        muestra(v);
        
        // EJERCICIO 5
        // -----------   
        // Escribe una función a la que se le pase un vector ordenado y un 
        // número y devuelva un nuevo vector de longitud una vez mayor con el
        // nuevo elemento intercalado en la posición correcta de manera que siga
        // ordenado ascendentemente:
        // Entrada: v1 {1, 2, 6, 7, 15, 25} num: 5
        // Salida: v2 {1, 2, 5, 6, 7, 15, 25}
        System.out.println("\nEjercicio 5");
        System.out.println("-----------");
        int v5[]={1, 2, 6, 7, 15, 25};
        int num=5;
        v=insertaNumEnV(v5, num);
        muestra(v);
    
        // EJERCICIO 6
        // -----------   
        // Escribe una función a la que se le pase un vector desordenado y  
        // devuelva un nuevo vector ordenado
        
        System.out.println("\nEjercicio 6");
        System.out.println("-----------");
        int v61[]={10, 2, 26, 7, 15, 25};
        int v62[]=burbuja(v61);
        muestra(v61);
        muestra(v62);
    }
    
    // ---------
    // FUNCIONES
    // ---------
    
    // Esta función es muy fácil, porque sabemos que los dos vectores tienen
    // el mismo tamaño.
    static int[] intercala(int v1[], int v2[]) {
        int cont=0;
        int[] v=new int[v1.length+v2.length];
        for (int i=0; i<v1.length; i++) {
            v[cont]=v1[i];
            cont++;
            v[cont]=v2[i];
            cont++;
        }
        return v;
    }
    
    // En esta tenemos el problema de que cuando acabemos de recorrer uno hay
    // que seguir con el otro y para eso hemos de saber cuál es el mayor de los
    // dos. Cuando terminemos de recorrer el primero seguiremos sólo con el otro
    static int[] intercala2(int v1[], int v2[]) {
        int cont=0;
        int mayor, menor, vMayor;
        mayor=v1.length;
        if (mayor<v2.length) {
            mayor=v2.length;
            menor=v1.length;
            vMayor=2;
        } else {
            menor=v2.length;
            vMayor=1;
        }
        int[] v=new int[mayor+menor];
        for (int i=0; i<menor; i++) {
            v[cont]=v1[i];
            cont++;
            v[cont]=v2[i];
            cont++;
        }
        
        if (vMayor==1) {
            for (int i=menor; i<mayor; i++) {
                v[cont]=v1[i];
                cont++;
            }
        } else {
            for (int i=menor; i<mayor; i++) {
                v[cont]=v2[i];
                cont++;
            }
        }
        return v;
    }
    
    // En este lo que hacemos es ir recorriendo los dos vectores de entrada y
    // comparando sus elementos. Se insertará el menor de los dos siempre para
    // mantener así el orden. Tenemos que ir recorriendo hasta que lleguemos al
    // final de uno de los dos y luego ya insertaremos el resto de elementos
    // del vector restante.
    static int[] joinSortedV(int [] v1, int[] v2) {
        boolean salir=false;
        int tamV1=v1.length;
        int tamV2=v2.length;
        int v[]=new int[tamV1+tamV2];
        int indiceV1=0, indiceV2=0, indiceV=0;
        while (! salir) {
            if (v1[indiceV1]<v2[indiceV2]) {
                v[indiceV]=v1[indiceV1];
                indiceV1++;
            } else {
                v[indiceV]=v2[indiceV2];
                indiceV2++;                
            }
            indiceV++;
            // está claro que acabaremos con un vector antes que con el otro, lo
            // que no sé es cuál de ellos. Dependerá del contenido de cada uno. 
            if (indiceV1==tamV1 || indiceV2==tamV2) {
                salir=true;
            }
        }
        // Ahora seguiremos con el que aún le queden elementos.
        if (indiceV1==tamV1) {
            for (int i=indiceV2; i<tamV2; i++) {
                v[indiceV]=v2[indiceV2];
                indiceV2++;
                indiceV++;
            }
        } else {
            for (int i=indiceV1; i<tamV1; i++) {
                v[indiceV]=v1[indiceV1];
                indiceV1++;
                indiceV++;
            }            
        }
        return v;
    }
    
    static int[] sumaV(int v1[], int v2[]) {
        int[] v=new int[v1.length];
        for (int i=0; i<v.length; i++) {
            v[i]=v1[i]+v2[i];
        }
        return v;
    }
    
    // En este caso iremos añadiendo todos los elementos menores que num, pero
    // una vez insertado en su sitio ya no tenemos que seguir comparando num
    // o lo insertaríamos de nuevo. Por eso usamos yaInsertado, para evitar esto
    static int[] insertaNumEnV(int[] vIn, int num) {
        boolean yaInsertado=false;
        int vOut[]=new int[vIn.length+1];
        int indiceVOut=0;
        for (int i=0; i<vIn.length; i++) {
            if (num>vIn[i] || yaInsertado) {
                vOut[indiceVOut]=vIn[i];
            } else {
                vOut[indiceVOut]=num;
                indiceVOut++;
                vOut[indiceVOut]=vIn[i];
                yaInsertado=true;
            }
            indiceVOut++;
        }
        return vOut;
    }
    
    static void muestra(int[] v) {
        System.out.print("{ ");
        for (int e:v) {
            System.out.print(e+" ");
        }
        System.out.println("}");
    }
    
    static int[] burbuja(int[] v) {
        int w[]=v.clone();
        int aux;
        int tope=w.length-1;
        while (tope>0) {
            for (int i=0; i<tope; i++) {
                if (w[i]>w[i+1]) {
                    aux=w[i+1];
                    w[i+1]=w[i];
                    w[i]=aux;
                }
            }
            tope--;
        }
        return w;
    }
}
