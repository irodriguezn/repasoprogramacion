package algoritmica;

/**
 *
 * @author nacho
 */
public class A6_MatricesBuclesAnidados {
    public static void main(String[] args) {
        
        // --------
        // MATRICES
        // --------
        
        // Una matriz es un vector de vectores
        // Por ejemplo, si yo tengo los vectores:
        // v1: {1, 2, 3} | v2: {3, 2, 1} | v3: {3, 1, 2}
        // Una matriz podría ser:
        // M1: {v1, v2, v3}
        // o directamente sin definir antes v1, v2,  y v3:
        // M1: { {1, 2, 3},  {3, 2, 1}, {3, 1, 2} }
        
        // Es decir, el primer elemento de M1 (posición 0) NO ES un entero sino 
        // un VECTOR de enteros y así sucesivamente.
        
        // Normalmente se suele representar como si fuera una matriz matemática
        // es decir con filas y columnas. Digamos que cada vector lo podemos ver
        // como si estuviera situado bajo el anterior. Cada vector es una FILA.
        
        // | 1 2 3 | -> v1
        // | 3 2 1 | -> v2
        // | 3 1 2 | -> v3
        
        // Para declarar una matriz de enteros de 3x3:
        
        int [][] m1={{1, 2, 3}, {3, 2, 1}, {3, 1, 2}};
        
        // o bien declararla primero e ir añadiéndole valores posteriormente:
        
        int [][] m2= new int[3][3];
        m2[0][0]=1;
        m2[0][1]=1;
        m2[0][2]=1;
        m2[1][0]=1;
        m2[1][1]=1;
        m2[1][2]=1;
        // etc;
        
        // o bien declarar los vectores "fila" y luego el que los contiene:
        int [] v1=new int [3];
        v1[0]=2; v1[1]=3; v1[2]=1;
        int [] v2={1, 1, 1};
        int [] v3={2, 3, 2};
        
        int m3[][]= {v1, v2, v3};
        
        //Para recorrer una matriz usaremos un bucle anidado habitualmente:
        
        // EJEMPLO 1
        // ---------
        System.out.println("EJEMPLO 1");
        System.out.println("---------");
        // con bucles for normales:
        for (int i=0; i<m1.length; i++) { // Este recorrerá las filas
            for (int j=0; j<m1[i].length; j++) { // Este las columnas
                System.out.print(m1[i][j]+ " ");
            }
            System.out.println("");
        }
        
        // IMPORTANTE FIJARSE en cómo sabemos el límite de la fila o de la
        // columna. m1.length es el número de filas y m1[0].length el número de
        // elementos de la fila 0. Por eso pongo m1[i].length, para que coja el
        // tamaño de la fila en la que está, pues realmente no tienen porqué ser
        // del mismo tamaño cada una de las filas.
        
        // EJEMPLO 2
        // ---------
        System.out.println("\nEJEMPLO 2");
        System.out.println("---------");
        // con bucles for each:
        for (int[] v:m1) { // Por cada vector en m1
            for (int num:v) { // Por cada elemento entero en v
                System.out.print(num+ " ");
            }
            System.out.println("");
        }
        
        // Vamos ahora a aprovechar este último código para crear una función
        // que muestre una matriz cualquiera. Llamaremos a la función mostrarM
        // EJEMPLO 3
        // ---------
        System.out.println("\nEJEMPLO 3");
        System.out.println("---------");
        mostrarM(m1);
        mostrarM(m2);
        
        // Vamos ahora a crear una función que genere una matriz aleatoria con
        // dígitos del 0 al 9 y tamaño nxn a la que llamaremos generaMatriz(n)
        // EJEMPLO 4
        // ---------
        System.out.println("EJEMPLO 4");
        System.out.println("---------");
        int m4[][]=generaMatriz(5); //genera una matriz aleatoria de 5x5
        mostrarM(m4);     
        
        // Vamos a crear ahora una función a la que le pasemos una matriz 
        // cuadrada de número de filas par y que nos devuelva el resultado de
        // sumar todos los números de las filas de arriba y restarle todos los
        // de la fila de abajo. Por ejemplo:
        //
        // | 1 2 | -> 1 + 2 = 3
        // | 3 2 | -> 3 + 2 = 5
        // Resultado: 3 - 5 = -2
        // Otro ejemplo: 
        // | 1 2 6 4| -> 13
        // | 3 2 5 7| -> 17
        // | 1 1 1 7| -> 10
        // | 1 2 1 7| -> 11
        // Resultado: 13 + 17 - 10 - 11 = 9
        // EJEMPLO 5
        // ---------
        System.out.println("EJEMPLO 5");
        System.out.println("---------");
        int[][] m5=generaMatriz(4); //genero una matriz de 4x4
        mostrarM(m5);
        System.out.println("Mitad de arriba menos mitad de abajo: " + restaMitad(m5));
        
        
        // EJERCICIO PROPUESTO 1: Haz lo mismo pero en lugar de arriba y abajo
        // resta la parte derecha a la parte izquierda.
        
        
        // Vamos a generar ahora una matriz con las tablas de multiplicar del 2 
        // al 9, es decir algo así:
        // |  2  3 ...  9|
        // |  4  6 ... 18|
        // |  6  9 ... 27|
        // |  8 12 ... 36|
        // | 10 15 ... 45|
        // | 12 18 ... 54|
        // | 14 21 ... 63|
        // | 16 24 ... 72|
        // etc.
        // EJEMPLO 6
        // ---------
        System.out.println("EJEMPLO 6");
        System.out.println("---------");
        int m6[][]=tablaMultiplicar();
        mostrarM(m6);
    
        // Vamos a crear ahora otra función mostrarM a la que pasaremos un segundo
        // parámetro que indicará el número máximo de dígitos que tendrá el mayor
        // número contenido en la matriz. Y mostrará la matriz de manera que a cada
        // número con menos dígitos le añada espacios para que quede todo alineado.
        // Aplicaremos este nuevo método a la tabla de multiplicar creada antes.
        // EJEMPLO 7
        // ---------
        System.out.println("EJEMPLO 7");
        System.out.println("---------");
        mostrarM(m6, 2);
    }
    
    // FUNCIONES ESTÁTICAS NECESARIAS
    // ------------------------------
    
    public static void mostrarM(int m[][]) {
        for (int[] v:m) { 
            for (int num:v) { 
                System.out.print(num + " ");
            }
            System.out.println("");
        }        
        System.out.println("");
    }

    public static void mostrarM(int m[][], int digitos) {
        int longitud, espaciosAñadir;
        String numConEspacios="";
        for (int[] v:m) { 
            for (int num:v) { 
                longitud=(num+"").length();
                if (longitud<digitos) {
                    espaciosAñadir=digitos-longitud;
                    numConEspacios=espacios(espaciosAñadir) + num;
                    System.out.print(numConEspacios + " ");
                } else {
                    System.out.print(num + " ");
                }
            }
            System.out.println("");
        }        
        System.out.println("");
    }    
    
    public static String espacios(int num) {
        String espacios="";
        for (int i=0; i<num; i++) {
            espacios+=" ";
        }
        return espacios;
    }
    
    public static int[][] generaMatriz(int n) {
        int m[][]=new int[n][n];
        for (int i=0; i<n; i++) {
            for (int j=0; j<n; j++) {
                m[i][j]=(int)(Math.random()*10);
            }
        }
        return m;
    }
    
    public static int restaMitad(int[][] m) {
        int mitad=m.length/2;
        int sumaArriba=0;
        int sumaAbajo=0;
        for (int i=0; i<mitad; i++) {
            for (int j=0; j<m.length; j++) {
                sumaArriba+=m[i][j];
            }
        }
        for (int i=mitad; i<m.length; i++) {
            for (int j=0; j<m.length; j++) {
                sumaAbajo+=m[i][j];
            }
        }        
        return sumaArriba-sumaAbajo;
    }
    
    public static int[][] tablaMultiplicar() {
        int[][] tabla=new int[10][8];
        for (int i=0; i<10; i++) {
            for (int j=0; j<8; j++) {
                tabla[i][j]=(i+1)*(j+2);
            }
        }
        return tabla;
    }
}
