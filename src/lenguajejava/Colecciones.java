package lenguajejava;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.HashSet;

/**
 *
 * @author nacho
 */
public class Colecciones {
    public static void main(String[] args) {
        
        // -----------
        // COLECCIONES
        // -----------
        
        // Las colecciones son estructuras dinámicas, es decir, algo parecido a
        // un vector, con datos consecutivos pero, a diferencia de este, en
        // estas podemos añadir elementos en tiempo de ejecución.
        
        LinkedList<Integer> numeros=new LinkedList<>();
        numeros.add(5);
        numeros.add(7);
        numeros.add(10);
        
        /*for (int i=0; i<numeros.size(); i++) {
            System.out.print(numeros.get(i)+ " ");
        }
        
        System.out.println("");
        for (int elemento:numeros) {
            System.out.print(elemento+" ");
        }*/
        int num;
        Iterator it=numeros.iterator();
        while (it.hasNext()) {
            num=(int)it.next();
            System.out.print(num+" ");
        }
        System.out.println("");
        numeros.add(6);
        numeros.add(15);
        numeros.add(4);
        numeros.add(6);
        numeros.remove(4);
        
        //{2,3,5}
        it=numeros.iterator();
        while (it.hasNext()) {
            num=(int)it.next();
            System.out.print(num+" ");
        }        
    }
}
