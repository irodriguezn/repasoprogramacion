package lenguajejava;

/**
 *
 * @author nacho
 */
public class ExcepcionesIntroduccion {
    public static void main(String[] args) {
        
        //-------------
        // EXCEPCIONES
        //-------------
        
        // Cuando se produce un error en tiempo de ejecución, la máquina virtual
        // de Java no no sabe exactamente qué hacer. No puede continuar el 
        // programa de forma normal así que lo que hace es crear una Excepcion.
        // Una Excepción es un objeto (como todo en Java). Ese objeto nos informa
        // de que se ha producido un error y nos da información sobre este.
        // De esa manera nosotros podemos preveer qué hacer en caso de errores y 
        // conseguir que nuestro programa continúe funcionando a pesar de estos.
        
        // Si no tratamos los errores el programa se interrumpirá y nos informará
        // de que se ha producido este. En programas complejos no es nada bueno
        // que el programa se interrumpa a causa de un error. Así que se hace
        // necesario preveerlos y tratarlos.
        
        // ¿QUÉ SE PUEDE HACER EN JAVA CUANDO SE PRODUCE UNA EXCEPCIÓN?
        // ------------------------------------------------------------
        // Si en una parte del programa, una clase, subclase, método, submétodo
        // o lo que sea se produce un error y nos llega, por tanto, una excepción
        // tenemos 3 posibilidades:
        
        // - No hacer nada -> El programa se interrumpe y nos informa del error
        // - Tratar la excepción (usar try-catch)
        // - Propagar la excepción hacia arriba (usar throws)
        
        // ¿QUÉ SIGNIFICA PROPAGAR LA EXCEPCIÓN HACIA ARRIBA?
        // --------------------------------------------------
        // En Java se pueden anidar métodos. O simplemente podemos estar llamando
        // a un método desde el programa principal. Si el error se produce dentro
        // del método más interno tiene la posibilidad de tratar él mismo el error
        // o delegar en el método-clase que le ha llamado usando la cláusula
        // throws. Es decir, puede pasarle la patata caliente a otro. Y este otro
        // método puede volver a hacer lo mismo. Tratar el error o pasarlo a quien
        // lo ha llamado, a su vez, a él.
        
        // VEÁMOSLO MEJOR CON EJEMPLOS
        // ---------------------------
        
        // Vamos a crear 3 métodos dividir. En uno de ellos no haremos nada
        // así que cuando lo llamemos con op2=0 el programa se parará y nos
        // avisará de que se ha producido una excepción.
        // En el siguiente trataremos la excepción en el propio método
        // Por último propagaremos la excepción hacia arriba. Diremos que es
        // un método que puede producir excepciones y desde el programa principal
        // lo llamaremos dos veces. Una vez trataremos el error y la otra no.
        
        // Descomenta cada llamada para observar la diferencia
        
        dividir1(10,0);
        // Esta llamada interrumpe el programa y nos dice qué ha pasado:
        // Exception in thread "main" java.lang.ArithmeticException: / by zero
        // y también en qué método se ha producido: dividir1 y dónde está ese
        // método: at lenguajejava.ExcepcionesIntroduccion.dividir1	
        // at lenguajejava.ExcepcionesIntroduccion.main
        
        //dividir2(10,0);
        // Esta llamada no interrumpe el programa. Nos muestra un mensaje de
        // error y sigue
        
        // Pasa exactamente lo mismo que cuando llamamos a dividir1 porque
        // ninguno se ha ocupado de tratar el error. De decirle qué tiene que
        // hacer si se produce.
        //dividir3(10,0);
        
        /*try {
            dividir3(10,0);        
        } catch (ArithmeticException e) {
            System.out.println("Error aritmético");
        }*/
        // En este pasa lo mismo que en la segunda llamada, sólo que esta vez
        // el error es tratado aquí y no en el mismo método.
        
        System.out.println("El programa continuaría por aquí");
    }
    
    // Método que no hace nada si se produce un error
    public static void dividir1(int op1, int op2) {
        int resultado=op1/op2;
        System.out.println("El resultado es: " + resultado);
    }
    
    // Método que trata el error
    public static void dividir2(int op1, int op2) {
        try {
            int resultado=op1/op2;
            System.out.println("El resultado es: " + resultado);        
        } catch (ArithmeticException e) {
            System.out.println("Error en la división");
        }
    }
    
    // Método que propaga los posibles errores hacia arriba
    public static void dividir3(int op1, int op2) throws ArithmeticException {
        int resultado=op1/op2;
        System.out.println("El resultado es: " + resultado);
    }    
}
