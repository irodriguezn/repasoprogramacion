
package lenguajejava;

import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author nachorod
 */
public class ColeccionesObjetos {
    public static void main(String[] args) {
        LinkedList<Personaje> personajes=new LinkedList<>();
        Personaje p=new Personaje("pepe", 100, 100);
        Personaje p2;
        personajes.add(p);
        p=new Personaje("maria", 100, 100);
        personajes.add(p);
        Iterator it=personajes.iterator();
        System.out.println(personajes);
        while (it.hasNext()) {
            p2=(Personaje)it.next();
            if (p2.getNombre().equals("pepe")) {
                p2.setResistencia(150);
            }
        }
        

        
        System.out.println(personajes);
        //System.out.println(p.toString());
    }
}

class Personaje {
    String nombre;
    int puntos;
    int resistencia;

    public Personaje(String nombre, int puntos, int resistencia) {
        this.nombre = nombre;
        this.puntos = puntos;
        this.resistencia = resistencia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPuntos() {
        return puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }

    public int getResistencia() {
        return resistencia;
    }

    public void setResistencia(int resistencia) {
        this.resistencia = resistencia;
    }

    @Override
    public String toString() {
        return "{"+  nombre +" "+ puntos + " " +resistencia + '}';
    }
    
}
