package lenguajejava;

import java.io.*;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nacho
 */
public class Archivos {
    public static void main(String[] args) {
        try {
            /*escribirArchivo();
            leerArchivo();*/
            primosN(10);
        } catch (IOException ex) {
            System.out.println("Error E/S");
        }
    }
    
    public static void escribirArchivo() {
        FileWriter fw=null;
        BufferedWriter bw=null;
        try {
            File f=new File("numeros.txt");
            fw = new FileWriter(f);
            bw= new BufferedWriter(fw);
            bw.write("10, 15, 20\r\n");
            bw.write("25, 39, 35");
        } catch (IOException ex) {
            System.out.println("No existe el fichero");
        } finally {
            try {
                bw.close();
                fw.close();
            } catch (IOException ex) {
                System.out.println("Error de E/S");
            }
        }
    }
    
    
    public static void leerArchivo() {
        FileReader fr=null;
        BufferedReader br=null;
        String linea="";
        int[] numeros=new int[3];
        try {
            fr = new FileReader("numeros.txt");
            br= new BufferedReader(fr);
            while (linea!=null) {
                linea=br.readLine();
                if (linea!=null) {
                    numeros=toVector(linea);
                    printVector(numeros);
                }
            }
        } catch (IOException ex) {
            System.out.println("No existe el fichero");
        } finally {
            try {
                br.close();
                fr.close();
            } catch (IOException ex) {
                System.out.println("Error de E/S");
            }
        }
    }    
    
    static int[] toVector(String linea) {
        StringTokenizer st= new StringTokenizer(linea);
        int size=st.countTokens();
        int[] numeros = new int[size];
        for (int i=0; i<size; i++) {
            numeros[i]=Integer.parseInt(st.nextToken(",").trim());
        }
        return numeros;
    }
    
    static void printVector(int[] numeros) {
        System.out.print("{ ");
        for (int i=0; i<numeros.length; i++) {
            System.out.print(numeros[i]+" ");
        }
        System.out.println("}");
    }
    
    static void primosN(int n) throws IOException {
        FileWriter fr= new FileWriter("primosN");
        BufferedWriter br=new BufferedWriter(fr);
        br.write(primos(n));
    }
    
    static String primos(int n) {
       
        return "";
    }
    
}
